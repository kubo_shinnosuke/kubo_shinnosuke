<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>${loginUser.account}の設定</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<h3>${loginUser.account}の設定</h3>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="settings" method="post">
			<br /> <input name="id" value="${editUser.id}" id="id" type="hidden" />
			<label for="account">アカウント名</label> <input name="account"
				value="${editUser.account}" /><br /> <label for="name">氏名</label>
			<input name="name" value="${editUser.name}" /><br /> <label
				for="branch">支店番号</label> <input name="branch"
				value="${editUser.branch}" /><br /> <label for="position">役職</label>
			<input name="position" value="${editUser.position}" /><br /> <label
				for="password">パスワード</label> <input name="password" type="password"
				id="password" /><br /> <br /> <input type="submit" value="登録" /><br />
			<a href="management">戻る</a>
		</form>
	</div>
</body>
</html>