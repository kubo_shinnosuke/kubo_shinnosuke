<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ホーム</title>
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<meta http-equiv="refresh"
					content="10;URL=http://localhost:8080/kubo_shinnosuke/login">
				<div align="left">
					ログインしてください。10秒後にログイン画面に移動します。 <br> 移動しない場合は<a
						href="http://localhost:8080/kubo_shinnosuke/login">ここ</a>をクリックしてください。
				</div>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="newMessage">新規投稿</a>
				<a href="management">ユーザー管理</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>

		<div class="messages">
			<c:if test="${ not empty loginUser }">
				<table border="1">
					<c:forEach items="${messages}" var="message">
						<tr>
							<th>
							<span class="message-area">
								<span class="account-name-date">
										<span class="account"><c:out value="${message.account}" /></span>
										<span class="name"><c:out value="${message.name}" /></span>
								</span>
								<span class="account-name-date">
										<span class="date">
										<fmt:formatDate value="${message.created_date}"
										pattern="yyyy/MM/dd HH:mm:ss" />
										</span>
								</span>
							</span>
								<div class="text">
									Title:
									<c:out value="${message.title}" />
									<br>
									<c:out value="${message.text}" />
								</div>

<!-- コメントDB検索してあるなら表示? -->
			<c:if test="${ not empty loginUser }">
				<c:forEach items="${comment}" var="comment">
							<span class="message-area">
								<span class="account-name-date">
										<span class="account"><c:out value="${comment.account}" /></span>
										<span class="name"><c:out value="${comment.name}" /></span>
								</span>
								<span class="account-name-date">
										<span class="date">
										<fmt:formatDate value="${comment.created_date}"
										pattern="yyyy/MM/dd HH:mm:ss" />
										</span>
								</span>
							</span>
							<div class="text">
									<c:out value="${comment.text}" />
								</div>
</c:forEach>
</c:if>







							</th>
						</tr>
					</c:forEach>
				</table>
			</c:if>
		</div>
	</div>
</body>
</html>

