<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ユーザー管理</title>
</head>
<body>
	<h3>ユーザー管理</h3>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<meta http-equiv="refresh"
					content="0;URL=http://localhost:8080/kubo_shinnosuke/login">
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="signup">ユーザー登録</a>
			</c:if>
		</div>

		<div class="users">
			<table border="1">
				<tr>
					<th>ID</th>
					<th>アカウント名</th>
					<th>氏名</th>
					<th>支店番号</th>
					<th>役職</th>
					<c:forEach items="${user}" var="user">
						<tr>
							<td><span class="id"> <c:out value="${user.id}" /></span></td>
							<td><span class="account"> <c:out
										value="${user.account}" /></span></td>
							<td><span class="name"><c:out value="${user.name}" /></span></td>
							<td><span class="branch"><c:out
										value="${user.branch}" /></span></td>
							<td><span class="position"><c:out
										value="${user.position}" /></span></td>
							<td><a href="settings?id=${user.id}">設定</a></td>
						</tr>
					</c:forEach>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>


