<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<title>ユーザー登録</title>
	</head>
	<body>
		<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="signup" method="post"> <br />

				<label for="name">名前</label><br />
				<input name="name" id="name" /><br />
				<br />

				<label for="account">アカウント名</label><br />
				<input name="account" id="account" /> <br />
				<br />

				<label for="password">パスワード</label><br />
				<input name="password" type="password" id="password" /> <br />
				<br />

				<label for="branch">支店</label><br />
				<input name="branch" id="branch" /> <br />
				<br />

				<label for="position">部署・役職</label><br />
				<input name="position" id="position" /> <br />
				<br />

				<input type="submit" value="登録" /> <br />
				<a href="management">戻る</a>
			</form>
		</div>
	</body>
</html>