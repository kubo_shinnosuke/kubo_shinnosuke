package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Management;
import exception.SQLRuntimeException;

public class ManagementDao {

	//public List<Management> getUserManagement(Connection connection, int num) {
	public List<Management> getUserManagement(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.account as account, ");
			sql.append("users.name as name, ");
			sql.append("users.branch as branch, ");
			sql.append("users.position as position ");
			sql.append("FROM users ");
			//sql.append("ORDER BY created_date DESC limit "/* + num*/);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Management> ret = toUserMessageList(rs);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Management> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<Management> ret = new ArrayList<Management>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String name = rs.getString("name");
				String branch = rs.getString("branch");
				String position = rs.getString("position");

				Management message = new Management();
				message.setId(id);
				message.setAccount(account);
				message.setName(name);
				message.setBranch(branch);
				message.setPosition(position);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}